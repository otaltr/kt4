import java.util.*;
import java.util.stream.Collectors;

public class Quaternion {

   private final double a1;
   private final double b1;
   private final double c1;
   private final double d1;

   static final double almostZero = 0.0000001;

   public Quaternion (double a, double b, double c, double d) {
      a1 = a;
      b1 = b;
      c1 = c;
      d1 = d;
   }

   public double getRpart() {
      return a1;
   }

   public double getIpart() {
      return b1;
   }

   public double getJpart() {
      return c1;
   }

   public double getKpart() {
      return d1;
   }

   @Override
   public String toString() {

      String rPart = Math.abs((int) a1) > almostZero ? String.valueOf((int) a1) : ""; // WE DO NOT WANT A "+" BEFORE A POSITIVE "REAL" PART
      String iPart = Math.abs((int) b1) > almostZero ? formatDouble(b1) + "i" : "";
      String jPart = Math.abs((int) c1) > almostZero ? formatDouble(c1) + "j" : "";
      String kPart = Math.abs((int) d1) > almostZero ? formatDouble(d1) + "k" : "";

      return rPart + iPart + jPart + kPart;

   }

   public String formatDouble(double n) {
      if (n < 0) {
         return String.valueOf((int) n);
      }
      return "+" + (int) n;
   }

   public static Quaternion valueOf (String s) {
      if (s.isEmpty()) {
         return new Quaternion(0, 0, 0, 0);
      }

      if (!isValidInput(s)) {
         throw new IllegalArgumentException("Input string is invalid.");
      };

      List<String> allTokens = getStringsFromTokens(s, "+-", true);

      ArrayList<String> quaternionElements = new ArrayList<>();

      for (int i = 0; i < allTokens.size(); i++) {

         String currentToken = allTokens.get(i);
         String nextToken = "";
         if (i != allTokens.size() - 1) {          // BUT IF WE ARE IN THE END OF STRING, DO NOT LOOK FOR ANOTHER TOKEN
             nextToken = allTokens.get(i + 1);
         }

         if (currentToken.equals("+")) {
            quaternionElements.add(nextToken);
         }
         else if (currentToken.equals("-")) {
            quaternionElements.add("-" + nextToken);        // ADD MINUS IN FRONT OF A TOKEN TO BE SUBTRACTED
         }
         else {
            if (quaternionElements.isEmpty()) {
               quaternionElements.add(currentToken);        // THE "REAL" PART OF THE QUATERNION IS FOUND
            }
         }
      }

      double a = 0.0;
      double b = 0.0;
      double c = 0.0;
      double d = 0.0;

      ArrayList<String> imaginaries = new ArrayList<>(Arrays.asList("i", "j", "k"));

      for (String element : quaternionElements) {
         List<String> tokens = getStringsFromTokens(element, "ijk",true); // SEPARATES THE VALUE AND IMAGINARY PART

         if (imaginaries.contains(tokens.get(tokens.size() - 1))) {        // IF ELEMENT CONTAINED AN IMAGINARY PART
            String imaginary = tokens.get(tokens.size() - 1);           // GET THE IMAGINARY PART
            switch (imaginary) {
               case "i" -> b = Double.parseDouble(tokens.get(0));
               case "j" -> c = Double.parseDouble(tokens.get(0));
               case "k" -> d = Double.parseDouble(tokens.get(0));
            }
         } else {
               a = Double.parseDouble(tokens.get(0)); // "REAL" PART DOES NOT CONTAIN AN AN IMAGINARY PART
         }
      }

      return new Quaternion(a, b, c, d);
   }

   private static boolean isValidInput(String input) {
      return isValidRealPart(input) && areValidImaginaryParts(input);

   }

   private static boolean areValidImaginaryParts(String input) {
      List<String> tokens = getStringsFromTokens(input, "+-ijk", true);

      ArrayList<String> possibleImaginaries = new ArrayList<>(Arrays.asList("i", "j", "k"));
      ArrayList<String> occuredImaginaries = new ArrayList<>();

      for (String token : tokens) {
         if (possibleImaginaries.contains(token)) {
            if (!occuredImaginaries.contains(token)) {
               occuredImaginaries.add(token);
            } else {                 // AN IMAGINARY PART HAS OCCURRED TWICE - ILLEGAL
               return false;
            }
         }
      }

      if (occuredImaginaries.size() == 3) {        // IF ALL IMAGINARIES HAVE OCCURRED, MUST MATCH WITH ORDER OF [i,j,k]
         return possibleImaginaries.equals(occuredImaginaries);
      } else if (occuredImaginaries.size() == 2) {              // ALL VALID COMBINATIONS ARE LISTED BELOW
         return occuredImaginaries.get(0).equals("i") && occuredImaginaries.get(1).equals("j") ||
                 occuredImaginaries.get(0).equals("i") && occuredImaginaries.get(1).equals("k") ||
                 occuredImaginaries.get(0).equals("j") && occuredImaginaries.get(1).equals("k");
      } else { // IF ZERO OR ONLY ONE IMAGINARY HAS OCCURRED, CAN'T BE INVALID
         return true;
      }
   }

   private static boolean isValidRealPart(String input) {
      List<String> allTokens = getStringsFromTokens(input, "+-", false);
      int realPartCount = 0;
      for (String token : allTokens) {
         try {
            Double.parseDouble(token);
            realPartCount++;              // PARSING WAS SUCCESSFUL - WAS A PURE INTEGER
         } catch (NumberFormatException ignored) {} // IGNORE PARSING ERROR - WAS AN IMAGINARY PART
      }
      return realPartCount == 1;  // AT LEAST 1 BUT NO MORE THAN 1 REAL PART MUST OCCUR
   }

   private static List<String> getStringsFromTokens(String formattedString, String delimiter, boolean returnDelimiters) {
      return Collections.list(new StringTokenizer(formattedString, delimiter, returnDelimiters)).stream()
                .map(token -> (String) token)
                .collect(Collectors.toList());
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(a1, b1, c1, d1);
   }

   public boolean isZero() {
      boolean a = Math.abs(a1) < almostZero;
      boolean b = Math.abs(b1) < almostZero;
      boolean c = Math.abs(c1) < almostZero;
      boolean d = Math.abs(d1) < almostZero;

      return a && b && c && d;
   }

   public Quaternion conjugate() {
      return new Quaternion(a1, -b1, -c1, -d1);
   }

   public Quaternion opposite() {
      return new Quaternion(-a1, -b1, -c1, -d1);
   }

   public Quaternion plus (Quaternion q) {
      double a = this.a1 + q.a1;
      double b = this.b1 + q.b1;
      double c = this.c1 + q.c1;
      double d = this.d1 + q.d1;
      return new Quaternion(a, b, c, d);
   }

   public Quaternion times (Quaternion q) {
      double a = this.a1 * q.a1 - this.b1 * q.b1 - this.c1 * q.c1 - this.d1 * q.d1;
      double b = this.a1 * q.b1 + this.b1 * q.a1 + this.c1 * q.d1 - this.d1 * q.c1;
      double c = this.a1 * q.c1 - this.b1 * q.d1 + this.c1 * q.a1 + this.d1 * q.b1;
      double d = this.a1 * q.d1 + this.b1 * q.c1 - this.c1 * q.b1 + this.d1 * q.a1;
      return new Quaternion(a, b, c, d);
   }

   public Quaternion times (double r) {
      return new Quaternion(a1 * r, b1 * r, c1 * r, d1 * r);
   }

   public Quaternion inverse() {
      double op = a1*a1 + b1*b1 + c1*c1 + d1*d1;
      if (op < almostZero) {
         throw new RuntimeException("Can't inverse a quaternion that equals to zero.");
      }
      double a = a1 / op;
      double b = -b1 / op;
      double c = -c1 / op;
      double d = -d1 / op;

      return new Quaternion(a, b, c, d);
   }

   public Quaternion minus (Quaternion q) {
      double a = this.a1 - q.a1;
      double b = this.b1 - q.b1;
      double c = this.c1 - q.c1;
      double d = this.d1 - q.d1;
      return new Quaternion(a, b, c, d);
   }

   public Quaternion divideByRight (Quaternion q) {
      try {
         Quaternion inverseQuaternion = q.inverse();
         return this.times(inverseQuaternion);
      } catch (RuntimeException e) {
         throw new RuntimeException("Can't inverse a quaternion that equals to zero.");
      }
   }

   public Quaternion divideByLeft (Quaternion q) {
      try {
         Quaternion inverseQuaternion = q.inverse();
         return inverseQuaternion.times(this);
      } catch (RuntimeException e) {
         throw new RuntimeException("Can't inverse a quaternion that equals to zero.");
      }
   }

   @Override
   public boolean equals (Object qo) {
      Quaternion q = (Quaternion) qo;

      boolean if1 = this.a1 - q.a1 < almostZero;
      boolean if2 = this.b1 - q.b1 < almostZero;
      boolean if3 = this.c1 - q.c1 < almostZero;
      boolean if4 = this.d1 - q.d1 < almostZero;

      return if1 && if2 && if3 && if4;
   }

   public Quaternion dotMult (Quaternion q) {
      Quaternion f1 = this.times(q.conjugate());
      Quaternion f2 = q.times(this.conjugate());
      Quaternion sum = f1.plus(f2);
      return sum.times(0.5);
   }

   @Override
   public int hashCode() {
      return Objects.hash(a1, b1, c1, d1);
   }

   public double norm() {
      return Math.sqrt(a1*a1+b1*b1+c1*c1+d1*d1);
   }

   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 0, 2., -2.);
      //Quaternion arv1 = new Quaternion (0., 0, 0., 0.);
      if (arg.length > 0)
        arv1 = valueOf (arg[0]);

      String s = "4i-3j-1";
      System.out.println(valueOf(s).toString());
   }
}
// end of file
